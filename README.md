# Meca500 Robot Arm SiLA Server

Open source SiLA driver for Mecademic / Meca500 Robot Arm maintained by Siobra.

## Usage

### List Commands
To display supported Commands use `-c yes` as an argument:

```bash
java -jar target/meca500-exec.jar -c yes
```

## Supported device models
Tested with SiLA 2 version ...

Tested with device models:
* ...

All product and company names are trademarks™ or registered® trademarks of their respective holders. Use of them does not imply any affiliation with or endorsement by them.
